<?php

/**
 * 
 */
class Validator{
	
	protected $_db;
	protected $errorHandler;
	protected $items;
	protected $rules = ['required', 'minlength', 'maxlength', 'email', 'alnum', 'alpha', 'digit', 'match', 'unique'];
	
	public $messages = [
	   'required' =>  'El campo :field es requerido',
	   'minlength' => 'El campo :field deberia tener un mínimo de :satisfier de largo',
	   'maxlength' => 'El campo :field deberia tener un maximo de  :satisfier de largo',
	   'email' => 'Esta no es una dirección de correo electrónico válida',
	   'alnum' => 'El campo :field debe ser alfanumérico',
	   'alpha' => 'El campo :field debe ser solo alfabético',
	   'digit' => 'El campo :field debe tener solo dígitos',
	   'match' => 'El campo :field debe ser igual que el campo :satisfier',
	   'unique' => 'El :field ya existe'
	];
	
	public function __construct(ErrorHandler $errorHandler) 
	{
		$this->_db = DB::getInstance();
		$this->errorHandler = $errorHandler;
	}
	
	public function check($items, $rules)
	{
		$this->items = $items;
		
	  foreach ($items as $item => $value) {
		  if (in_array($item, array_keys($rules))) 
		  {
			$this->validate([
			   'field' => $item,
			   'value' => $value,
			   'rules' => $rules[$item]  
			]);  
		  }
	  }	
	  return $this;
	}
	
	public function fails()
	{
	  return $this->errorHandler->hasErrors();	
	}
	
	public function errors()
	{
	  return $this->errorHandler;	
	}
	
	protected function validate($item)
	{
	   $field = $item['field'];
	   foreach ($item['rules'] as $rule => $satisfier) 
	   {
		 if (in_array($rule, $this->rules)) 
		 {
		   if (!call_user_func_array([$this, $rule], [$field, $item['value'], $satisfier])) 
		   {
			 $this->errorHandler->addError(
			  str_replace([':field',':satisfier'], [$field, $satisfier], $this->messages[$rule]), 
			  $field
			  );  
		   }
		 }   
	   }	

	}
	
	protected function required($field, $value, $satisfier)
	{
	  return !empty(trim($value));	
	}
	
	protected function minlength($field, $value, $satisfier)
	{
	  return mb_strlen($value) >= $satisfier;	
	}
	
	protected function maxlength($field, $value, $satisfier)
	{
	  return mb_strlen($value) <= $satisfier;	
	}
	
	protected function email($field, $value, $satisfier)
	{
	  return filter_var($value, FILTER_VALIDATE_EMAIL);	
	}
	
	protected function alnum($field, $value, $satisfier)
	{
	  return ctype_alnum($value);	
	}
	
	protected function alpha($field, $value, $satisfier)
	{
	  return ctype_alpha($value);	
	}
	
	protected function digit($field, $value, $satisfier)
	{
	  return ctype_digit($value);	
	}
	
	protected function match($field, $value, $satisfier)
	{
	  return $value === $this->items[$satisfier];	
	}
	
	protected function unique($field, $value, $satisfier)
	{
	  return $this->_db->has($field, $value, $satisfier);	
	  //return $this->_db->get($satisfier, "*", [$field => $value]);
	  //return $this->_db->exists($satisfier, [$field => $value]) ? true : false;	
	}
}



?>
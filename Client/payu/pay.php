<?php
  //Check if init.php exists
  if(!file_exists('../../core/binit.php')){
  	header('Location: ../../install/');        
    exit;
  }else{
    require_once '../../core/binit.php';	
  }
  require_once '../../lib/PayU.php';

  /* try {
    PayU::$apiKey = "4Vj8eK4rloUd272L48hsrarnUA"; //Ingrese aquí su propio apiKey.
    PayU::$apiLogin = "pRRXKOl8ikMmt9u"; //Ingrese aquí su propio apiLogin.
    PayU::$merchantId = "508029"; //Ingrese aquí su Id de Comercio.
    PayU::$language = 'es'; //Seleccione el idioma.
    PayU::$isTest = true; //Dejarlo True cuando sean pruebas.
  
    // URL de Pagos
    Environment::setPaymentsCustomUrl("https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi");
    // URL de Consultas
    Environment::setReportsCustomUrl("https://sandbox.api.payulatam.com/reports-api/4.0/service.cgi");
    // URL de Suscripciones para Pagos Recurrentes
    Environment::setSubscriptionsCustomUrl("https://sandbox.api.payulatam.com/payments-api/rest/v4.9/");
  
    $reference = 'payment_test_00000001';
    $value = 10000;
  
    $parameters = [
      PayUParameters::ACCOUNT_ID => '512321',
      PayUParameters::REFERENCE_CODE => $reference,
      PayUParameters::DESCRIPTION => 'Payment test',
  
      PayUParameters::VALUE => $value,
      PayUParameters::TAX_VALUE => 0,
      PayUParameters::TAX_RETURN_BASE => 0,
      PayUParameters::CURRENCY => "COP",
  
      PayUParameters::BUYER_NAME => "Julian Andres Chilito",
      PayUParameters::BUYER_EMAIL => "julian.chilitoz@gmail.com",
      PayUParameters::BUYER_CONTACT_PHONE => "573182290635",
      PayUParameters::BUYER_DNI => "1151955227",
      PayUParameters::BUYER_STREET => "calle 100",
      PayUParameters::BUYER_STREET_2 => "5555487",
      PayUParameters::BUYER_CITY => "Cali",
      PayUParameters::BUYER_STATE => "Valle del cauca",
      PayUParameters::BUYER_COUNTRY => "CO",
      PayUParameters::BUYER_POSTAL_CODE => "000000",
      PayUParameters::BUYER_PHONE => "573182290635",
  
      // -- pagador --
      //Ingrese aquí el nombre del pagador.
      PayUParameters::PAYER_NAME => "First name and second payer name",
      //Ingrese aquí el email del pagador.
      PayUParameters::PAYER_EMAIL => "payer_test@test.com",
      //Ingrese aquí el teléfono de contacto del pagador.
      PayUParameters::PAYER_CONTACT_PHONE => "7563126",
      //Ingrese aquí el documento de contacto del pagador.
      PayUParameters::PAYER_DNI => "5415668464654",
      //Ingrese aquí la dirección del pagador.
      PayUParameters::PAYER_STREET => "calle 93",
      PayUParameters::PAYER_STREET_2 => "125544",
      PayUParameters::PAYER_CITY => "Bogota",
      PayUParameters::PAYER_STATE => "Bogota",
      PayUParameters::PAYER_COUNTRY => "CO",
      PayUParameters::PAYER_POSTAL_CODE => "000000",//1113658784 - restrepo
      PayUParameters::PAYER_PHONE => "7563126",
  
      // -- Datos de la tarjeta de crédito --
      //Ingrese aquí el número de la tarjeta de crédito
      PayUParameters::CREDIT_CARD_NUMBER => "4305923429917815",
      //Ingrese aquí la fecha de vencimiento de la tarjeta de crédito
      PayUParameters::CREDIT_CARD_EXPIRATION_DATE => "2023/04",
      //Ingrese aquí el código de seguridad de la tarjeta de crédito
      PayUParameters::CREDIT_CARD_SECURITY_CODE=> "725",
      //Ingrese aquí el nombre de la tarjeta de crédito
      //VISA||MASTERCARD||AMEX||DINERS
      PayUParameters::PAYMENT_METHOD => "VISA",
  
      //Ingrese aquí el número de cuotas.
      PayUParameters::INSTALLMENTS_NUMBER => "1",
      //Ingrese aquí el nombre del pais.
      PayUParameters::COUNTRY => PayUCountries::CO,
  
      //Session id del device.
      PayUParameters::DEVICE_SESSION_ID => "vghs6tvkcle931686k1900o6e1",
      //IP del pagadador
      PayUParameters::IP_ADDRESS => "127.0.0.1",
      //Cookie de la sesión actual.
      PayUParameters::PAYER_COOKIE=>"pt1t38347bs6jc9ruv2ecpv7o2",
      //Cookie de la sesión actual.
      PayUParameters::USER_AGENT=>"Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"
    ];
  
    $response = PayUPayments::doAuthorizationAndCapture($parameters);
  
    if ($response) {
      echo '<pre>';
      var_dump($response);
      echo '</pre>';
    }
  } catch (Exception $e) {
    var_dump($e->getMessage());
  } */

<?php
  if(!file_exists('../../core/binit.php')){
  	header('Location: ../../install/');        
    exit;
  }else{
    require_once '../../core/binit.php';	
  }
  
  $client = new Client();

  if (!$client->isLoggedIn()) {
    Redirect::to('../index.php');	
  }

  $ApiKey = "4Vj8eK4rloUd272L48hsrarnUA";
  $merchant_id = $_REQUEST['merchantId'];
  $referenceCode = $_REQUEST['referenceCode'];
  $TX_VALUE = $_REQUEST['TX_VALUE'];
  $New_value = number_format($TX_VALUE, 1, '.', '');
  $currency = $_REQUEST['currency'];
  $transactionState = $_REQUEST['transactionState'];
  $firma_cadena = "$ApiKey~$merchant_id~$referenceCode~$New_value~$currency~$transactionState";
  $firmacreada = md5($firma_cadena);
  $firma = $_REQUEST['signature'];
  $reference_pol = $_REQUEST['reference_pol'];
  $cus = $_REQUEST['cus'];
  $extra1 = $_REQUEST['description'];
  $pseBank = $_REQUEST['pseBank'];
  $lapPaymentMethod = $_REQUEST['lapPaymentMethod'];
  $transactionId = $_REQUEST['transactionId'];

  if ($_REQUEST['transactionState'] == 4 ) {
    $estadoTx = "Transacción aprobada";

    $jobid = $referenceCode;
    try {
      //Insert
      $Insert = DB::getInstance()->insert('transactions', array(
        'membershipid' => $jobid,
        'freelancerid' => $client->data()->clientid,
        'paymentid' => 'PAYU',
        'hash' => 4,
        'payment' => $TX_VALUE,
        'complete' => 1,
        'transaction_type' => 3,
        'date_added' => date('Y-m-d H:i:s')
        ));	 

      //Update Job
      $Update = DB::getInstance()->update('job',[
        'featured' => 1,
        'featured_date' => date('Y-m-d H:i:s')
      ],[
        'jobid' => $jobid
      ]);
    } catch(Exception $e) {
      //var_dump($e);
    }

  }

  else if ($_REQUEST['transactionState'] == 6 ) {
    $estadoTx = "Transacción rechazada";
  }

  else if ($_REQUEST['transactionState'] == 104 ) {
    $estadoTx = "Error";
  }

  else if ($_REQUEST['transactionState'] == 7 ) {
    $estadoTx = "Transacción pendiente";
  }

  else {
    $estadoTx=$_REQUEST['mensaje'];
  }


  if (strtoupper($firma) == strtoupper($firmacreada)) {
  ?>
    <h2>Resumen Transacción</h2>
    <table>
    <tr>
    <td>Estado de la transaccion</td>
    <td><?php echo $estadoTx; ?></td>
    </tr>
    <tr>
    <tr>
    <td>ID de la transaccion</td>
    <td><?php echo $transactionId; ?></td>
    </tr>
    <tr>
    <td>Referencia de la venta</td>
    <td><?php echo $reference_pol; ?></td>
    </tr>
    <tr>
    <td>Referencia de la transaccion</td>
    <td><?php echo $referenceCode; ?></td>
    </tr>
    <tr>
    <?php
    if($pseBank != null) {
    ?>
      <tr>
      <td>cus </td>
      <td><?php echo $cus; ?> </td>
      </tr>
      <tr>
      <td>Banco </td>
      <td><?php echo $pseBank; ?> </td>
      </tr>
    <?php
    }
    ?>
    <tr>
    <td>Valor total</td>
    <td>$<?php echo number_format($TX_VALUE); ?></td>
    </tr>
    <tr>
    <td>Moneda</td>
    <td><?php echo $currency; ?></td>
    </tr>
    <tr>
    <td>Descripción</td>
    <td><?php echo ($extra1); ?></td>
    </tr>
    <tr>
    <td>Entidad:</td>
    <td><?php echo ($lapPaymentMethod); ?></td>
    </tr>
    </table>
    <a href="/Client/joblist.php">Regresar</a>
  <?php
  }
  else
  {
  ?>
    <h1>Error validando firma digital.</h1>
  <?php
  }
  ?>
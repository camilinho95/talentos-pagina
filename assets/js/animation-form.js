$primero = false;

$("#next-1").click(function(event) {
    event.preventDefault();
    if ($("#title-form").val().length == 0) {
        alert("Ingrese su profesión o talento");
    } else {
        $primero = true;
        if ($primero == false) {
            $(".content-form-animated-I").hide();
        } else {
            $(".content-form-animated-I").show();
            $(".header-jobs").css("height", "140vh");
            $("#btn-next-step-1").hide();
        }
    }
});

$("#next-2").click(function(event) {
    event.preventDefault();
    //if($('#value-form').val().length ==0 || $('#date-form1').val().length == 0 || $('#date-form2').val().length == 0 ){
    if ($("#value-form").val().length == 0) {
        alert("Por favor llene todos los campos antes de continuar");
    } else {
        $primero = true;
        if ($primero == false) {
            $(".content-form-animated-II").hide();
        } else {
            $(".content-form-animated-II").show();
            $(".header-jobs").css("height", "160vh");
            $("#btn-next-step-2").hide();
            $("#descNotification").animate({ margin: "100px" });
            $("#descNotification").modal("show");
        }
    }
});

$("#addform").submit(function(event) {
    let formData = new FormData();
    var form = $(this).serializeArray();

    event.preventDefault();
    $.each(form, function(key, input) {
        formData.append(input.name, input.value);
    });

    var file_data = $('input[name="inscriptionFile"]')[0].files;


    if (file_data.length > 0) formData.append("inscriptionFile", file_data[0]);

    if ($("#logged").val() == "1") {
        $.ajax({
            type: "POST",
            url: $(this).attr("action"), //'postnewJob.php',
            processData: false,
            contentType: false,
            data: formData,
            dataType: "json",
            success: function(data) {
                console.log("Data: ", data);
                //$('#addform').trigger("reset");
                $("#summernote").summernote("code", "");
                $(".statusFormJob").html(
                    '<span style="color:white;">Su solicitud ha sido añadida con éxito</span>'
                );
            },
            error: function(data) {
                console.log("error: ", data);
            },
        });
    } else {
        $("#myModal").animate({ margin: "100px" });
        $("#myModal").modal("show");
    }
});

function submitRegisterForm() {
    var $reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+.)+[A-Z]{2,4}$/i;
    var $nombre = $("#inputNombre").val();
    var $email = $("#inputEmail").val();
    var $phone = $("#inputPhone").val();
    var $user = $nombre.substring(0, 4) + $phone;
    //var $photo = $("#file").val();
    var $nomPartes = [];
    $nomPartes = $nombre.split(" ");
    var $contrasena = $nomPartes[0] + "123$";
    var $contrasena2 = $nomPartes[0] + "123$";
    var token = $("#token").val();

    if ($nombre.trim() == "") {
        alert("Por favor ingrese su nombre.");
        $("#inputNombre").focus();
        return false;
    } else if ($email.trim() == "") {
        alert("Por favor ingrese su email");
        $("#inputEmail").focus();
        return false;
    } else if (!$reg.test($email)) {
        alert("Por favor ingrese una dirección de correo valida");
        $("#inputEmail").focus();
        return false;
    } else if ($user.trim() == "") {
        alert("Por favor ingrese un nombre de usuario");
        $("#inputUser").focus();
        return false;
    } else if ($contrasena[0].trim() == "") {
        alert("Por favor ingrese su contraseña");
        $("#inputContrasena").focus();
        return false;
    } else if ($phone.trim() == "") {
        alert("Por favor ingrese su teléfono");
        $("#inputPhone").focus();
        return false;
    } else if ($contrasena[0].trim() != $contrasena2[0].trim()) {
        alert("Sus contraseñas no coinciden");
        $("#inputContrasena").focus();
        $("#inputContrasena2").focus();
        return false;
    } else {
        console.log(
            "registerFrmSubmit=1&name=" +
            $nombre +
            "&phone=" +
            $phone +
            "&email=" +
            $email +
            "&username=" +
            $user +
            "&contrasena=" +
            $contrasena +
            "&token=" +
            token
        );
        console.log($nomPartes);
        $.ajax({
            type: "POST",
            url: "postnewClient.php",
            data: "registerFrmSubmit=1&name=" +
                $nombre +
                "&phone=" +
                $phone +
                "&email=" +
                $email +
                "&username=" +
                $user +
                "&contrasena=" +
                $contrasena +
                "&token=" +
                token,
            beforeSend: function() {
                $(".submitBtn").attr("disabled", "disabled");
                $(".modal-body").css("opacity", ".5");
            },
            success: function(msg) {
                console.log(msg);
                if (msg.indexOf("ok1") > -1) {
                    $("#inputNombre").val("");
                    $("#inputEmail").val("");
                    $("#inputUser").val("");
                    $("#inputContrasena").val("");
                    $("#inputContrasena2").val("");
                    $("#idclient").attr("value", msg.substr(4));
                    $("#correo").attr("value", $email);
                    $("#inputPhone").attr("value", $phone);
                    $("#logged").attr("value", "1");
                    $("#myModal").modal("hide");
                    $(".statusFormJob").html(
                        '<span style="color:white;">Su registro fue exitoso. Bienvenido a TalentosOnline, por favor vuelva a enviar su formulario para añadir su trabajo</span>'
                    );
                    $(".statusFormJob").html(
                        '<span style="color:white;">Su registro fue exitoso. Bienvenido a TalentosOnline</span>'
                    );

                    $("#addform").submit();
                } else {
                    $(".statusMsg").html(
                        '<span style="color:red;">Ha ocurrido un problema: ' +
                        msg +
                        "</span>"
                    );
                    $(".submitBtn").removeAttr("disabled");
                    $(".modal-body").css("opacity", "");
                }
            },
        });
    }
}

function submitTalentForm() {
    var $reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+.)+[A-Z]{2,4}$/i;
    var $nombre = $("#inputNombre").val();
    var $email = $("#inputEmail").val();
    var $user = $("#inputUser").val();
    var $contrasena = $user + "123$"; //$('#inputContrasena').val();
    var $contrasena2 = $user + "123$"; //$('#inputContrasena2').val();
    var token = $("#token").val();

    if ($nombre.trim() == "") {
        alert("Por favor ingrese su nombre.");
        $("#inputNombre").focus();
        return false;
    } else if ($email.trim() == "") {
        alert("Por favor ingrese su email");
        $("#inputEmail").focus();
        return false;
    } else if (!$reg.test($email)) {
        alert("Por favor ingrese una dirección de correo valida");
        $("#inputEmail").focus();
        return false;
    } else if ($user.trim() == "") {
        alert("Por favor ingrese un nombre de usuario");
        $("#inputUser").focus();
        return false;
    }
    /*else if ($contrasena.trim() == ''){
			alert('Por favor ingrese su contraseña');
			$('#inputContrasena').focus();
			return false;
		}else if ($contrasena.trim() != $contrasena2.trim()){
			alert('Sus contraseñas no coinciden');
			$('#inputContrasena').focus();
			$('#inputContrasena2').focus();
			return false;
		}*/
    else {
        $.ajax({
            type: "POST",
            url: "postnewTalent.php",
            data: "registerFrmSubmit=1&name=" +
                $nombre +
                "&email=" +
                $email +
                "&username=" +
                $user +
                "&contrasena=" +
                $contrasena +
                "&token=" +
                token,
            beforeSend: function() {
                $(".submitBtn").attr("disabled", "disabled");
                $(".modal-body").css("opacity", ".5");
            },
            success: function(msg) {
                console.log(msg);
                if (msg.indexOf("ok") > -1) {
                    $("#inputNombre").val("");
                    $("#inputEmail").val("");
                    $("#inputUser").val("");
                    //$('#inputContrasena').val('');
                    //$('#inputContrasena2').val('');
                    $("#idtalent").attr("value", msg.substr(msg.lastIndexOf(":") + 1));
                    $("#logged").attr("value", "1");
                    $("#myModal").modal("hide");
                    //$('.statusFormJob').html('<span style="color:white;">Su registro fue exitoso. Bienvenido a TalentosOnline, por favor vuelva a enviar su formulario para añadir su trabajo</span>');
                    $(".statusFormJob").html(
                        '<span style="color:white;">Su registro fue exitoso. Bienvenido a TalentosOnline</span>'
                    );

                    $("#addform").submit();
                } else {
                    $(".statusMsg").html(
                        '<span style="color:red;">Ocurrió un error durante su registro</span>'
                    );
                    $(".submitBtn").removeAttr("disabled");
                    $(".modal-body").css("opacity", "");
                }
            },
        });
    }
}
     <?php echo $google_analytics; ?>
     <!-- ==============================================
	 Footer Section
	 =============================================== -->
     <div class="footer">
         <style>
             .btn-whats {
                    display:block;
                    width:70px;
                    height:70px;
                    color#fff;
                    position: fixed;
                    right:20px;
                    bottom:20px;
                    border-radius:50%;
                    line-height:80px;
                    text-align:center;
                    z-index:999;
            }
         </style>
         <div class="btn-whats">
            <a href="https://api.whatsapp.com/send?phone=573157713452" target="_blank">
            <img src="/assets/img/btn_whatsapp.png" alt="">
            </a>
        </div>

	  <div class="container">
	   <div class="row">
	  
	    <div class="col-md-4 col-sm-6 text-left">
	     <h4 class="heading no-margin"><?php echo $lang['about']; ?> <?php echo $lang['us']; ?></h4>
		 <hr class="mint">
		 <h4 class="heading no-margin"><?php echo $lang['freelancers']; ?></h4>
		 <p>Es una plataforma que ofrece la oportunidad de conectar personas que requieran un
servicio y las que lo realizan con éxito. Alguien sabe hacer lo que usted necesita y alguien busca lo
que usted sabe hacer.
		     <?php// echo $footer_about; ?></p>
		 <h4 class="heading no-margin"><?php echo "Por qu&eacute;?"; ?></h4>
		 <p>En un mundo donde todo cambia y el tiempo parece no alcanzar, adicionando que las
cosas no son permanentes, la opción de requerir lo justo en tiempo y más allá de necesitar
oportunamente, es fundamental tener opciones que garanticen un trabajo bien hecho, nosotros
nos encargamos de suministrarle el talento adecuado que cada persona ha desarrollado con
conocimiento, con experiencia y con placer; para solucionar su necesidad especifica.</p>
	    </div><!-- /.col-md-4 -->
	   
	   
	    <div class="col-md-2 col-sm-6 text-left">
	     <h4 class="heading no-margin"><?php echo $lang['company']; ?></h4>
		 <hr class="mint">
		 <div class ="no-padding">
		  <a href="index.php"><?php echo $lang['home']; ?></a>
		  <!--
		  <a href="jobs.php"><?php echo $lang['jobs']; ?></a>
		  <a href="services.php"><?php echo $lang['services']; ?></a>-->
		  <a href="about.php"><?php echo $lang['about']; ?></a>
		  <a href="how.php"><?php echo $lang['how']; ?> <?php echo $lang['it']; ?> <?php echo $lang['works']; ?></a>
		 </div>
	    </div><!-- /.col-md-2 -->	
		
		<div class="col-md-3 col-sm-6 text-left">
	     <h4 class="heading no-margin"><?php echo $lang['other']; ?> <?php echo $lang['services']; ?></h4>
		 <hr class="mint">
		 <div class="no-padding">
		  <a href="login.php"><?php echo $lang['login']; ?></a>
		  <a data-toggle="modal" data-target="#Modal"><?php echo $lang['register']; ?></a>		 
		  <a href="faq.php"><?php echo $lang['faq']; ?></a>	
		  <a href="contact.php"><?php echo $lang['contact']; ?></a>		 	 
		 </div>
	    </div><!-- /.col-md-3 -->	
		
	    <div class="col-md-3 col-sm-6 text-left">
	    <h4 class="heading no-margin"><?php echo $lang['contact']; ?> <?php echo $lang['us']; ?></h4>
		<hr class="mint">
		 <div class="no-padding">
		   <a><?php echo $contact_location; ?></a>
		   <a><?php echo $contact_phone; ?></a>
		   <a href="mailto:<?php echo $contact_email; ?>"><?php echo $contact_email; ?></a>	  
		  </div>
		 </div><!-- /.col-md-3 -->
		 
	    </div><!-- /.row -->
	   <div class="clearfix"></div>
	  </div><!-- /.container-->
	 </div><!-- /.footer -->	
	 
	 <div class="modal fade" id="Modal" role="dialog">
				<div class="modal-dialog">	
					<div align="center">
						<div class="modal-content" 
						style="width: 400px;
							height: 300px;
							margin-top: 0px;">
							<div class="modal-header" style="padding-top: 10px;">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Elija una opcíon</h4>
							</div>
							<div class="modal-body" style="
								padding-top: 0px;
							">
								<div>
									<div>
										<a 
										href="jobpostnotlogged.php" class="kafe-btn kafe-btn-mint " data-animation="bounceIn" data-timeout="400" style="padding:20px 31px">
												Quiero contratar
										</a>
									</div>
									<div style="margin-top: 3em;">
										<a href="servicepostnotLogged.php" class="kafe-btn kafe-btn-mint  " data-animation="bounceIn" data-timeout="400" style="padding:20px 31px">
												Quiero trabajar
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	 
	 <!-- ==============================================
	 Bottom Footer Section
	 =============================================== -->	
     <footer id="main-footer" class="main-footer">
	  <div class="container">
	   <div class="row">
	   
	    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		 <ul class="social-links">
		  <li><a href="<?php echo $facebook; ?>"><i class="fa fa-facebook fa-fw"></i></a></li>
		 
		  <li><a href="<?php echo $instagram; ?>"><i class="fa fa-instagram fa-fw"></i></a></li>
		 
		 </ul>
		</div>
	    <!-- /.col-sm-4 -->
		
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 revealOnScroll" data-animation="bounceIn" data-timeout="200">
		 <div class="img-responsive text-center">
		     <img src="https://talentosonline.co/assets/img/talentoslogoBlanco.png" style="width: 100px;height: 100px;margin-top: -10px;">
       	</div><!-- End image-->
		</div>
		<!-- /.col-sm-4 -->
		
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-right revealOnScroll" data-animation="slideInRight" data-timeout="200">
		 <p><?php auto_copyright('','All Rights Reserved'); ?> </p>
		</div>
		<!-- /.col-sm-4 -->
				
	   </div><!-- /.row -->
	  </div><!-- /.container -->
	 </footer><!-- /.footer -->  
	 
     <a id="scrollup">Scroll</a>
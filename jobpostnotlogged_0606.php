<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
	header('Location: install/');        
    exit;
}else{
 require_once 'core/frontinit.php';	
}

?>
?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="es"> 
<!--<![endif]-->
    <link href="assets/css/nostyle.css" rel="stylesheet" type="text/css"/>
	    <!-- Bootstrap 3.3.6-->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Font-awesome 4.5.0-->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Data Tables -->
        <link href="assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Summernote -->
        <link  href="assets/css/summernote.css" rel="stylesheet" type="text/css" />
	 <!-- Bootstrap Datetimepicker CSS -->
    <link href="assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap Select CSS-->
    <link  href="assets/css/bootstrap-select.css" rel="stylesheet" type="text/css" />

    <!-- Include header.php. Contains header content. -->
    <?php include ('includes/template/header.php'); ?> 

    <body class="greybg">
	
     <!-- Include navigation.php. Contains navigation content. -->
 	 <?php include ('includes/template/navigation.php'); ?> 	 
	 
     <!-- ==============================================
	 Header
	 =============================================== -->	 
      <header class="header-jobs" style="

      background: linear-gradient(
          rgba(34,34,34,0.7), 
          rgba(34,34,34,0.7)
        ), url('assets/img/jobs/1489408561.jpg') no-repeat center center fixed;
      background-size: cover;
      background-position: center center;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      color: #fff;
      height: 80vh;
      width: 100%;
      "><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <script language="javascript">alert("Este sitio utiliza tus datos personales para ofrecerte un mejor servicio, al continuar estas aceptando esta condici�n");</script>
      <div class="container">
	         
	           <div class="row">
	               <div class="content">
	             <div class="box-header">
	                <h3 class="box-title">Describa el trabajo que necesita</h3>
                  <!--<h3 class="box-title"><?php //echo $lang['add']; ?> <?php //echo $lang['job']; ?></h3>-->
                </div>
                
              	 <form role="form" method="post" id="addform"> 
                  
                  <div class="form-group">  
                    <label><?php echo $lang['title']; ?></label>
                     <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-info"></i></span>
                        <input type="text" name="title" class="form-control" placeholder="<?php echo $lang['title']; ?>" value="<?php echo escape(Input::get('title')); ?>" id="title-form"/>
                      </div>
                  </div>
                  
                  <div class="form-group">  
                    <label><?php echo $lang['country']; ?></label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-info"></i></span>
                        <select name="country" class="form-control">
                          <option value="Colombia">Colombia</option>
                        </select>   
                      </div>         
                  </div> 
                  
                  <div class="form-group">  
                      <label><?php echo $lang['category']; ?></label>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-pencil-square"></i></span>
                          <select name="category" type="text" class="form-control">
                      <?php
                      $query = DB::getInstance()->get("category", "*", ["AND" => ["active" => 1, "delete_remove" => 0]]);
                      if ($query->count()) {
                      $categoryname = '';
                      $x = 1;
                      foreach ($query->results() as $row) {
                          echo $categoryname .= '<option value = "' . $row->catid . '">' . $row->name . '</option>';
                          unset($categoryname); 
                          $x++;
                        }
                      }
                      ?> 
                          </select>
                        </div>
                  </div>
                  <div class="button-next-step box-footer" id="btn-next-step-1"> <!-- BOTON SIGUIENTE PASO -->
                      <button name="data" class="btn btn-primary" id="next-1"><?php echo $lang['next_step']; ?></button>
                  </div>
                  
                  <!--div class="content-form-animated-I form-group">  
                    <label><?php echo $lang['job']; ?> <?php echo $lang['type']; ?></label>
                        <div class="radio">
                            <label><input type="radio" name="job_type" checked="checked" value="Fixed Price"><?php echo $lang['fixed_price']; ?></label>
                        </div>                    
                  </div--> 
                  <input type="hidden" name="job_type" value="Fixed Price">
                  
                  <div class="content-form-animated-I form-group">  
                    <label><?php echo $lang['budget']; ?></label>
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-money"></i></span>
                      <input type="text" name="budget" class="form-control" placeholder="<?php echo $lang['budget']; ?>" value="<?php echo escape(Input::get('budget')); ?>" id="value-form"/>
                    </div>
                  </div>       

                  <div class="content-form-animated-I form-group">
                     <label for="dtp_input1"><?php echo $lang['estimated']; ?> <?php echo $lang['start']; ?> <?php echo $lang['date']; ?></label>
                      <div class="input-group date form_datetime">
                        <input name="start_date" class="form-control" type="text" value="" id="date-form1" readonly>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-remove"></i></span>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                      </div>
                      <input type="hidden" id="dtp_input1" value="" /><br/>
                      <input name="mirror_field_start" type="hidden" id="mirror_field_start" class="form-control" readonly />
                      <input name="mirror_field_start_date" type="hidden" id="mirror_field_start_date" class="form-control" readonly />
                  </div> 
                  
                  <div class="content-form-animated-I form-group">
                     <label for="dtp_input1"><?php echo $lang['estimated']; ?> <?php echo $lang['end']; ?> <?php echo $lang['date']; ?></label>
                      <div class="input-group date form_datetime">
                        <input name="end_date" class="form-control" type="text" value="" id="date-form2" readonly>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-remove"></i></span>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                      </div>
                      <input type="hidden" id="dtp_input1" value="" /><br/>
                      <input name="mirror_field_start" type="hidden" id="mirror_field_start" class="form-control" readonly />
                      <input name="mirror_field_start_date" type="hidden" id="mirror_field_start_date" class="form-control" readonly />
                  </div> 
                  
                  <div class="content-form-animated-I button-next-step box-footer" id="btn-next-step-2"> <!-- BOTON SIGUIENTE PASO -->
                      <button name="data" class="btn btn-primary" id="next-2"><?php echo $lang['next_step']; ?></button>
                  </div>  
                    <!--Skills-->
                  <div class="content-form-animated-II form-group">	
				    <label><?php echo $lang['job']; ?> <?php echo $lang['skills']; ?></label>
				   <div class="input-group">
                    <?php
					$query = DB::getInstance()->get("skills", "*", ["ORDER" => "name ASC"]);
					if ($query->count()) {
					 foreach($query->results() as $row) {
					 	$names[] = $row->name;
					 }			
					}
					foreach($names as $key=>$name){
					   echo $skills .= '<div class="radio"> <label> <input type="radio" name="skill" value = "'.$name.'" data-tokens="'.$name.'" >'.$name.'</label> </div>';
					  unset($skills);
					  unset($name);
					}	
							
					 ?>
				  <!-- <select class="selectpicker form-control" name="skills_name[]" type="text" title="Choose one of the following..." data-live-search="true" data-width="30%" data-selected-text-format="count > 3" multiple="multiple">
					 	
					</select>-->
				   </div>
				  </div>    
                  <br/>                          
                  
                  <div class="content-form-animated-II form-group">  
                      <label><?php echo $lang['job']; ?> <?php echo $lang['description']; ?></label>
                      <textarea type="text" id="summernote" name="description" class="form-control"></textarea>
                  </div>
                
                           
                  <div class="content-form-animated-II box-footer">
                      <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
                      <?php
                            if ($client->isLoggedIn()){ ?>
                            <input type="hidden" name="logged" value="1" id="logged"/>
                            <input type="hidden" name="idclient" value="<?php
                            echo $client->data()->clientid."" ?>" id="idclient"/>
                            <input type="hidden" name="correo" value="<?php $query = DB::getInstance()->get("settings", "*", ["id" => 1]);
                                if ($query->count()) {
                                    foreach($query->results() as $row) {
 	                                    $correo = $row->mail;
                                    }			
                                }
                                echo $correo." ";
                            ?>" id="correo"/>
                      <?php }else{ ?>
                            <input type="hidden" name="logged" value="0" id="logged"/>
                            <input type="hidden" name="idclient" value="" id="idclient"/>
                            <input type="hidden" name="correo" value="" id="correo"/>
                      <?php }
                      ?>
                      <button type="submit" name="data" class="btn btn-primary full-width"><?php echo $lang['submit']; ?></button>
                  </div>
                 </form> 
                 <p class="statusFormJob"></p>
             </div><!-- /.row -->
          </div><!-- /.content -->
                    <div class="modal" id="myModal">
                        <div class="modal-dialog">
                            <div class="modal-content">
      
                            <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title">Formulario de Registro</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
        
                            <!-- Modal body -->
                                <div class="modal-body">
                                    <p class="statusMsg"></p>
                                    <form role="form">
                    					<div class="form-group">
                        					<label for="inputNombre">Nombre completo</label>
                        					<input type="text" class="form-control" id="inputNombre" placeholder="Ingrese su nombre completo"/>
                    					</div>
                    					<div class="form-group">
                        					<label for="inputEmail">Email</label>
                        					<input type="email" class="form-control" id="inputEmail" placeholder="Ingrese su email"/>
                    					</div>
                    					<div class="form-group">
                        					<label for="inputPhone">Tel&eacute;fono</label>
                        					<input type="number" class="form-control" id="inputPhone" placeholder="Ingrese su tel�fono"/>
                        				</div>
                        			</form>
                        		</div>
        
                            <!-- Modal footer -->
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                	<button type="button" class="btn btn-primary submitBtn" onclick="submitRegisterForm()">Enviar</button>
                                </div>
        
                            </div>
                        </div>
                    </div>
	    </div><!-- /.container -->

     </header><!-- /header -->
<!-- Include footer.php. Contains footer content. -->	
	  <?php include 'includes/template/footer.php'; ?>	
	 
     <!-- ==============================================
	 Scripts
	 =============================================== -->
	 
     <!-- jQuery 2.1.4 -->
     <script src="assets/js/jQuery-2.1.4.min.js" type="text/javascript"></script>
     <!-- Bootstrap 3.3.6 JS -->
     <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
     
     <!-- AdminLTE App -->
    <script src="assets/js/app.min.js" type="text/javascript"></script>
     
     <!-- Waypoints JS -->
     <script src="assets/js/waypoints.min.js" type="text/javascript"></script>
     <!-- Kafe JS -->
     <script src="assets/js/kafe.js" type="text/javascript"></script>
    <!-- Summernote WYSIWYG-->
    <script src="assets/js/summernote.min.js" type="text/javascript"></script> 
    <script src="assets/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script> 
    <script src="assets/js/bootstrap-datetimepicker.js" type="text/javascript"></script> 
        
     <script src="assets/js/animation-form.js" type="text/javascript"></script>
     
    <script type="text/javascript">
        $(".form_datetime").datetimepicker({
            format: "dd MM yyyy",
            autoclose: true,
            todayBtn: true,
            pickerPosition: "bottom-left"
        });
    </script> 
     <script>
        $(document).ready(function() {
        $('#summernote').summernote({
          height: 300,                 // set editor height
    
          minHeight: null,             // set minimum height of editor
          maxHeight: null,             // set maximum height of editor
    
          focus: true,                 // set focus to editable area after initializing summernote
        });    
        });
    </script>
</body>
</html>
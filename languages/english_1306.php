<?php
/* 
------------------
Language: English
------------------
*/

$lang = array();

// Errors
$lang['required_error'] = 'El campo :field es requerido';
$lang['minlength_error'] = 'El campo :field deberia tener un mínimo de :satisfier de largo';
$lang['maxlength_error'] = 'El campo :field deberia tener un maximo de  :satisfier de largo';
$lang['email_error'] = 'Esta no es una dirección de correo electrónico válida.';
$lang['alnum_error'] = 'El campo :field debe ser alfanumérico';
$lang['alpha_error'] = 'El campo :field debe ser solo alfabético';
$lang['digit_error'] = 'El campo :field debe tener solo dígitos';
$lang['match_error'] = 'El campo :field debe ser igual que el campo :satisfier';
$lang['unique_error'] = 'El :field ya existe';


$lang['hasError'] = '¡Error!';
$lang['noError'] = '¡Éxito!';

// Login Error
$lang['login_error'] = 'Usuario o contraseña invalido.';
// Forgot Error
$lang['forgot_error'] = 'Correo electrónico no existe';
$lang['forgot_error_one'] = 'Hemos enviado un correo electrónico a';
$lang['forgot_error_two'] = 'Haga clic en el enlace de restablecimiento de contraseña en el correo electrónico para generar una nueva contraseña.';
// Reset Error
$lang['reset_error'] = 'Estás usando la misma contraseña que antes';
$lang['notoken_error'] = 'Token no coincidente o Token no válido';
$lang['reset_success'] = 'Actualizá correctamente su contraseña. Ahora puede iniciar sesión.';
// Has Error
$lang['has_Error'] = 'Comprueba si has llenado todos los campos con información válida y vuelve a intentarlo. Gracias.';
// Saved Success
$lang['saved_success'] = 'Los detalles se han guardado correctamente.';
// Updated Success
$lang['updated_success'] = 'Los detalles se han actualizado con éxito.';
// Updated Success
$lang['sent_success'] = 'El mensaje se ha enviado correctamente.';
// Posted Error
$lang['posted_Error'] = 'Ya ha publicado una propuesta para este trabajo.';
// Payed Error
$lang['payed_Error'] = 'Ya has pagado por este Hito.';
// Canceled Error
$lang['cancelled_Error'] = 'Ha cancelado el pago de este hito.';
// Error Error
$lang['error_Error'] = 'Algo salió mal.';
// Profile Image Errors
$lang['image_select'] = 'Por favor, seleccione la imagen! Vuelve a intentarlo.';
$lang['image_profile_format'] = 'Formatos de archivo no válidos ..! Vuelve a intentarlo. Los formatos aceptados son jpg, png, gif, bmp y el tamaño debe ser 280 * 280 ';
$lang['image_format'] = 'Formatos de archivo no válidos ..! Vuelve a intentarlo. Los formatos aceptados son jpg, png, gif, bmp y el tamaño debe ser 1920 * 1280';
$lang['image_upload'] = 'Fallo la carga de la imagen. Vuelve a intentarlo.';
$lang['pass_Error'] = 'Su contraseña es incorrecta.';

// Navbar
$lang['languages'] = 'Idiomas';

// Home Page + Form Fields
$lang['login'] = 'Iniciar sesión';
$lang['username'] = 'Nombre de usuario';
$lang['password'] = 'Contraseña';
$lang['confirm_password'] = 'Confirmar contraseña';
$lang['register'] = 'Registro';
$lang['full_name'] = 'Nombre completo';
$lang['email'] = 'Email';
$lang['forgot_password'] = '¿Se te olvidó tu contraseña?';
$lang['reset_password'] = 'Restablecer la contraseña';
$lang['remember_me'] = 'Recuérdame';
$lang['submit'] = 'Enviar';

// Form Fields
$lang['name'] = 'Nombre';
$lang['phone'] = 'Teléfono';
$lang['phone_contact'] = 'Contacto telefónico';
$lang['address'] = 'Dirección';
$lang['date_added'] = 'Fecha Agregada';
$lang['date'] = 'Fecha';
$lang['added'] = 'Agregada';
$lang['title'] = 'Título';
$lang['description'] = 'Descripción';
$lang['quotes'] = 'Citas';
$lang['author_name'] = 'Nombre del autor';
$lang['url'] = 'URL';
$lang['category'] = 'Categoría';
$lang['budget'] = 'Presupuesto';

$lang['action'] = 'Acción';
$lang['view'] = 'Ver';
$lang['edit'] = 'Editar';
$lang['delete'] = 'Borrar';
$lang['back'] = 'espalda';
$lang['mark_as_done'] = '¡Marcar como hecho!';
$lang['update_results'] = 'Lo que hice hoy y los resultados que obtuve';
$lang['update-problems'] = 'Problemas o Desafíos que surgieron.';
$lang['update_questions'] = 'Preguntas que tengo.';
$lang['password'] = 'Contraseña';
$lang['current_password'] = 'contraseña actual';
$lang['new_password'] = 'nueva contraseña';
$lang['confirm_new_password'] = 'Confirmar nueva contraseña';
$lang['user_id'] = 'Identidad de usuario';
$lang['joined'] = 'Unido';
$lang['user_type'] = 'Tipo de usuario';
$lang['account'] = 'Cuenta';
//Overview
$lang['location'] = 'Ubicación';
$lang['city'] = 'Ciudad';
$lang['country'] = 'País';
$lang['rate'] = 'Tarifa';
$lang['rate_hour'] = 'Tarifa / hora';
$lang['website'] = 'Sitio web';
$lang['about'] = 'Acerca de';
$lang['education'] = 'Educación';
$lang['work'] = 'Trabajo';
$lang['experience'] = 'Experiencia';
$lang['awards'] = 'Premios';
$lang['and'] = 'y';
$lang['achievements'] = 'Logros';
$lang['date'] = 'Fecha';
$lang['completed'] = 'Terminado';
$lang['review'] = 'revisión';
$lang['reviews'] = 'Comentarios';
$lang['us'] = 'Nosotros';
$lang['earned'] = 'Ganado';
$lang['ratings'] = 'Calificaciones';
$lang['success'] = 'Éxito';
$lang['project'] = 'Proyecto';
//Mailbox
$lang['folders'] = 'Carpetas';
$lang['compose'] = 'Componer';
$lang['inbox'] = 'Bandeja de entrada';
$lang['sent'] = 'Expedido';
$lang['favorites'] = 'Favoritos';
$lang['trash'] = 'Basura';
$lang['message_body'] = 'Cuerpo del mensaje';
$lang['message'] = 'Mensaje';
$lang['new'] = 'Nuevo';
$lang['to'] = 'A';
$lang['select'] = 'Seleccionar';
$lang['subject'] = 'Tema';
$lang['mail'] = 'Correo';
$lang['mailbox'] = 'Buzón';
$lang['favorite'] = 'Favoritos';
$lang['star'] = 'Estrella';
$lang['starred'] = 'Sembrado de estrellas';
$lang['not_starred'] = 'No está estrellado';
$lang['from'] = 'De';
$lang['time'] = 'Hora';
$lang['send'] = 'Enviar';
$lang['restore'] = 'Restaurar';

//Invite/Job
$lang['invite'] = 'Invitación';
$lang['invites'] = 'Invita';
$lang['invitation'] = 'Invitación';
$lang['an'] = 'un';
$lang['estimated'] = 'Estimado';
$lang['end'] = 'Fin';
$lang['type'] = 'Tipo';
$lang['fixed_price'] = 'Precio Fijo - Pago por el proyecto. Requiere especificaciones detalladas.';
$lang['make'] = 'Hacer';
$lang['public'] = 'Público';
$lang['not_public'] = 'No público';
$lang['make_public'] = 'Haga que este trabajo sea público para que otros Talentos puedan encontrarlo y aplicarlo.';
$lang['hide'] = 'Esconder';
$lang['acceptance'] = 'Aceptación';
$lang['accepted'] = 'Aceptado';
$lang['declined'] = 'Rechazado';
$lang['be'] = 'Ser';
$lang['assigned'] = 'Asignado';
$lang['proposal'] = 'Propuesta';
$lang['proposals'] = 'Propuestas';
$lang['posted'] = 'Al corriente';
$lang['unassign'] = 'Cancelar la asignación';

//Job Board
$lang['milestones'] = 'Hitos';
$lang['discussions'] = 'Discusiones';
$lang['tasks'] = 'Tareas';
$lang['files'] = 'Archivos';
$lang['bugs'] = 'Errores';
$lang['links'] = 'Vínculos';
$lang['funds'] = 'Fondos';
$lang['cost'] = 'Costo';
$lang['milestone'] = 'Hito';
$lang['close'] = 'Cerca';
$lang['start'] = 'comienzo';
$lang['funded'] = 'Fundado';
$lang['not'] = 'No';
$lang['pay'] = 'Paga';
$lang['with'] = 'con';
$lang['paypal'] = 'Paypal';
$lang['payments'] = 'Pagos';
$lang['payment'] = 'Pago';
$lang['pay'] = 'Paga';

//New Words
$lang['pass'] = 'Paso';
$lang['between'] = 'Entre';
$lang['you'] = 'Usted';
$lang['paid'] = 'pagado';
$lang['for'] = 'para';
$lang['this'] = 'esta';
$lang['complete'] = 'Completar';
$lang['in_complete'] = 'Incompleto';
$lang['stripe'] = 'Raya';
$lang['someone'] = 'Alguien';
$lang['else'] = 'más';
$lang['still'] = 'todavía';
$lang['status'] = 'Estado';
$lang['update'] = 'Actualizar';
$lang['task'] = 'Tarea';
$lang['progress'] = 'Progreso';
$lang['percent'] = 'Por ciento';
$lang['upload'] = 'Subir';
$lang['uploaded'] = 'Subido';
$lang['preview'] = 'Avance';
$lang['size'] = 'tamaño';
$lang['link'] = 'Enlazar';
$lang['go'] = 'Ir';
$lang['to'] = 'A';
$lang['bug'] = 'Error';

$lang['priority'] = 'Prioridad';
$lang['low'] = 'Bajo';
$lang['medium'] = 'Medio';
$lang['high'] = 'Alto';
$lang['severity'] = 'Gravedad';
$lang['minor'] = 'Menor';
$lang['major'] = 'Mayor';
$lang['show'] = 'Espectáculo';
$lang['stopper'] = 'Tapón';
$lang['must'] = 'Debe';
$lang['fixed'] = 'Fijo';
$lang['pending'] = 'Pendiente';
$lang['reproducibility'] = 'Reproducibilidad';
$lang['bug_desc'] = 'Escribe una descripción detallada del error';
$lang['bug_repro'] = 'Pasos realizados para producir este error.';
$lang['reporter'] = 'Reportero';
$lang['comments'] = 'Comentarios';

//Jobs
$lang['applicants'] = 'Solicitantes';
$lang['bookmark'] = 'Marcador';
$lang['it\'s'] = 'sus';
$lang['free'] = 'Gratis';
$lang['rate/hr'] = 'Tarifa / hora';
$lang['keywords'] = 'Palabras clave';
$lang['or'] = 'o';
$lang['company'] = 'Empresa';

$lang['limit'] = 'Límite';
$lang['limits'] = 'Límites';

//Front-end
$lang['how'] = 'Cómo';
$lang['it'] = '';
$lang['works'] = 'Trabajar';
$lang['faq'] = 'Preguntas más frecuentes';
$lang['contact'] = 'contacto';
$lang['signup'] = 'Regístrate';
$lang['header'] = 'Encabezamiento';
$lang['featured'] = 'Destacados';
$lang['page'] = 'Página';
$lang['browse'] = 'Vistazo';
$lang['more'] = 'Más';
$lang['popular'] = 'Populares';
$lang['categories'] = 'Categorías';
$lang['tagline'] = 'Lema';
$lang['position'] = 'Posición';
$lang['item'] = 'ít.';
$lang['order'] = 'Orden';
$lang['drag'] = 'Arrastrar';
$lang['drop'] = 'soltar';
$lang['pick'] = 'Recoger';
$lang['icon'] = 'Icono';
$lang['sub'] = 'Sub';
$lang['testimonies'] = 'Testimonios';
$lang['testimony'] = 'Testimonio';
$lang['stats'] = 'Estadísticas';
$lang['hello'] = 'Hola';
$lang['our'] = 'Nuestra';
$lang['timeline'] = 'Cronograma';
$lang['sections'] = 'Secciones';
$lang['map'] = 'Mapa';
$lang['iframe'] = 'Iframe';
$lang['have'] = 'Tener';
$lang['questions'] = 'Preguntas';
$lang['footer'] = 'Pie de página';
$lang['social'] = 'Social';
$lang['icons'] = 'Iconos';
$lang['other'] = 'Otro';
$lang['frontend'] = 'Interfaz';
$lang['id'] = 'CARNÉ DE IDENTIDAD';
$lang['secret'] = 'Secreto';
$lang['download'] = 'Descargar';
$lang['currency'] = 'Moneda';
$lang['code'] = 'Código';
$lang['symbol'] = 'símbolo';
$lang['default'] = 'Defecto';
$lang['pricing'] = 'Precio';
$lang['membership'] = 'Afiliación';
$lang['yes'] = 'Sí';
$lang['no'] = 'No';
$lang['price'] = 'Precio';
$lang['rollover'] = 'Dese la vuelta';
$lang['buy'] = 'Comprar';
$lang['can'] = 'Poder';
$lang['able'] = 'poder';
$lang['see'] = 'ver';
$lang['additional'] = 'adicional';
$lang['members'] = 'Miembros';
$lang['number'] = 'Número';
$lang['of'] = 'De';
$lang['agency'] = 'Agencia';
$lang['agencies'] = 'Agencias';
$lang['set'] = 'conjunto';

//Milestone
$lang['delete_milestone'] = '¿Estás seguro de que quieres eliminar este Hito?';
// Add Task Error
$lang['addTask_Error'] = 'Los detalles se han agregado correctamente.';
// Not Task Error
$lang['notTask_Error'] = 'La función de tarea no funcionó.';
// Updated Task Error
$lang['updateTask_Error'] = 'Los detalles se han actualizado correctamente.';
//Delete Task
$lang['delete_task'] = '¿Está seguro de que desea eliminar esta tarea?';
// Add File Error
$lang['addFile_Error'] = 'Los detalles se han agregado correctamente.';
// Not File Error
$lang['notFile_Error'] = 'La función de archivo no funcionó.';
// Update File Error
$lang['updateFile_Error'] = 'Los detalles se han actualizado correctamente.';
//Delete File
$lang['delete_file'] = '¿Estás seguro de que quieres eliminar este archivo?';
// Add Link Error
$lang['addLink_Error'] = 'Los detalles se han agregado correctamente.';
// Not File Error
$lang['notLink_Error'] = 'La función de enlace no funcionó.';
// Update File Error
$lang['updateLink_Error'] = 'Los detalles fueron actualizados exitosamente.';
//Delete Link
$lang['delete_link'] = '¿Seguro que quieres eliminar este enlace?';
//Delete Testimony
$lang['delete_testimony'] = '¿Seguro que quieres eliminar este testimonio?';
//Delete Team
$lang['delete_team'] = '¿Estás seguro de que quieres eliminar este equipo?';
//Delete Timeline
$lang['delete_timeline'] = '¿Estás seguro de que quieres eliminar esta línea de tiempo?';
//Delete Client Section
$lang['delete_client_section'] = '¿Está seguro de que desea eliminar esta sección de cliente?';
//Delete Freelancer Section
$lang['delete_freelancer_section'] = '¿Estás seguro de que quieres eliminar esta sección de Talentos?';
//Delete Currency
$lang['delete_currency'] = '¿Seguro que quieres eliminar esta moneda?';
//Delete Currency
$lang['delete_membership'] = '¿Seguro que quieres eliminar esta suscripción?';

// Bugs Errors
$lang['addBug_Error'] = 'Los detalles se han agregado correctamente.';
$lang['notBug_Error'] = 'Función de error no funcionó.';
$lang['updateBug_Error'] = 'Los detalles se han actualizado correctamente.';
$lang['delete_bug'] = '¿Estás seguro de que quieres eliminar este Bug?';
$lang['bug_fixed'] = '¿Estás seguro de que este Bug ha sido arreglado?';
$lang['bug_pending'] = '¿Estás seguro de que este Bug está pendiente?';

//New New
// Canceled Error
$lang['cancelled_Payment'] = 'Ha cancelado este pago.';
$lang['left'] = 'Izquierda';
$lang['bids_Error'] = 'No tienes ofertas pendientes. Renueve su membresía para publicar una propuesta.';
$lang['sponsor'] = 'Patrocinador';
$lang['front'] = 'Frente';
$lang['in'] = 'en';
$lang['days'] = 'Día(s)';
$lang['boost'] = 'Aumentar';
$lang['assign'] = 'Asignar';
$lang['transaction'] = 'Transacción';
$lang['boosting'] = 'Impulsando';
$lang['on'] = 'En';
$lang['unread'] = 'No leído';
$lang['messages'] = 'Mensajes';
$lang['amount'] = 'Cantidad';
$lang['outstanding'] = 'Excepcional';
$lang['monthly'] = 'Mensual';
$lang['recap'] = 'Resumen';
$lang['finance'] = 'Financiar';
$lang['total'] = 'Total';
$lang['available'] = 'Disponible';
$lang['recent'] = 'Reciente';
$lang['received'] = 'Recibido';
$lang['finished'] = 'Terminado';
$lang['done'] = 'Hecho';
$lang['opened'] = 'Abrió';
$lang['yet'] = 'Todavía';
$lang['awarded'] = 'Galardonado';
//Job Done
$lang['done_job'] = '¿Estás seguro de que este trabajo se ha completado?';
//Job Open
$lang['open_job'] = '¿Estás seguro de que quieres abrir este trabajo?';

//New New Words
$lang['ratings'] = 'Calificaciones';
$lang['the'] = 'el';
$lang['cooperation'] = 'Cooperación';
$lang['communication'] = 'Comunicación';
$lang['adherence'] = 'Adherencia';
$lang['schedule'] = 'Programar';
$lang['availability'] = 'Disponibilidad';
$lang['quality'] = 'Calidad';
$lang['share'] = 'Compartir';
$lang['score'] = 'Puntuación';
$lang['now'] = 'Ahora';
$lang['requirements'] = 'Requisitos';
$lang['reasonable'] = 'Razonable';
$lang['deadlines'] = 'Plazos';
$lang['by'] = 'por';
$lang['rating'] = 'clasificación';
$lang['percentage'] = 'Porcentaje';
$lang['fee'] = 'Cuota';
$lang['taken'] = 'tomado';
$lang['earnings'] = 'Ganancias';
$lang['user'] = 'Usuario';
$lang['reports'] = 'Informes';
$lang['report'] = 'Informe';
$lang['reported'] = 'Informó';
$lang['analytics'] = 'Analítica';
//Reports
$lang['delete_report'] = '¿Seguro que desea eliminar este informe?';
$lang['delete_report_message'] = '¿Seguro que quieres borrar este mensaje?';


//New Words

// Mem Error
$lang['mem_error'] = 'La suscripción predeterminada no se ha establecido, por lo que no puede registrarse. Gracias';
// Mem Select Error
$lang['mem_set'] = 'Debe establecerse la membresía predeterminada para que los independientes puedan registrarse.';
$lang['note'] = 'Nota';
$lang['withdrawal'] = 'retirada';
$lang['method'] = 'método';
$lang['scheduling'] = 'Programación';
$lang['schedule'] = 'programar';
$lang['withdraw'] = 'retirar';
$lang['withdrawals'] = 'retiros';
$lang['confirm'] = 'confirmar';
$lang['setup'] = 'preparar';
$lang['month'] = 'mes';
$lang['paid'] = 'pagado';
$lang['search'] = 'buscar';
$lang['term'] = 'término';
$lang['top'] = 'parte superior';
$lang['down'] = 'abajo';
$lang['trending'] = 'tendencias';
$lang['get'] = 'obtener';
$lang['quote'] = 'Citar';

//Active
$lang['activate'] = 'Activar';
$lang['deactivate'] = 'Desactivar';
$lang['active'] = 'Activo';
$lang['in_active'] = 'En activo';
$lang['accept'] = 'Aceptar';
$lang['decline'] = 'Disminuci�n';


//Javascript Confirm Box Messages
//Freelancer
$lang['delete_freelancer'] = '¿Estás seguro de que quieres eliminar este Talento?';
$lang['activate_freelancer'] = '¿Estás seguro de que quieres activar este Talento?';
$lang['deactivate_freelancer'] = '¿Estás seguro de que quieres desactivar este Talento?';
//Client
$lang['delete_client'] = '¿Estás seguro de que desea eliminar este cliente?';
$lang['activate_client'] = '¿Estás seguro de que desea activar este cliente?';
$lang['deactivate_client'] = '¿Estás seguro de que desea desactivar este cliente?';
//Category
$lang['delete_category'] = '¿Seguro que quieres eliminar esta categoría?';
$lang['activate_category'] = '¿Seguro que quieres activar esta categoría?';
$lang['deactivate_category'] = '¿Estás seguro de que desea desactivar esta categoría?';
//Job
$lang['delete_job'] = '¿Está seguro de que desea eliminar este trabajo?';
$lang['activate_job'] = '¿Está seguro de que desea activar este trabajo?';
$lang['deactivate_job'] = '¿Está seguro de que desea desactivar este trabajo?';
$lang['make_public'] = '¿Estás seguro de que quieres hacer este trabajo público?';
$lang['hide_public'] = '¿Está seguro de que desea ocultar este trabajo de público?';
$lang['decline_job'] = '¿Estás seguro de que quieres rechazar esta oferta de trabajo?';
$lang['accept_job'] = '¿Estás seguro de que quieres aceptar esta oferta de trabajo?';
$lang['assign_job'] = '¿Estás seguro de que quieres asignar este trabajo a este Talento?';
$lang['unassign_job'] = '¿Estás seguro de que quieres asignar este trabajo a este Talento?';
//Bid
$lang['delete_proposal'] = '¿Seguro que quieres eliminar esta propuesta?';
$lang['activate_proposal'] = '¿Estás seguro de que quieres activar esta propuesta?';
$lang['deactivate_proposal'] = '¿Está seguro de que desea desactivar esta propuesta?';
//Portfolio
$lang['delete_portfolio'] = '¿Estás seguro de que quieres eliminar este Portafolio?';
$lang['activate_portfolio'] = '¿Estás seguro de que quieres activar este Portfolio?';
$lang['deactivate_portfolio'] = '¿Estás seguro de que quieres desactivar este Portfolio?';
//Service
$lang['delete_service'] = '¿Está seguro de que desea eliminar este servicio?';
$lang['activate_service'] = '¿Está seguro de que desea activar este servicio?';
$lang['deactivate_service'] = '¿Está seguro de que desea desactivar este servicio?';
//Team
$lang['delete_teammate'] = '¿Estás seguro de que quieres eliminar este Team Mate?';
$lang['activate_teammate'] = '¿Estás seguro de que deseas activar este Team Mate?';
$lang['deactivate_teammate'] = '¿Estás seguro de que quieres desactivar este Team Mate?';
//Team
$lang['delete_skill'] = '¿Estás seguro de que quieres eliminar esta Habilidad?';
$lang['activate_skill'] = '¿Estás seguro de que quieres activar esta habilidad?';
$lang['deactivate_skill'] = '¿Estás seguro de que quieres desactivar esta habilidad?';
//Inbox
$lang['delete_inbox'] = '¿Seguro que quieres moverte a la Papelera?';
$lang['delete_sent'] = '¿Seguro que quieres moverte a la Papelera?';
$lang['delete_trash'] = '¿Estás seguro de que quieres eliminar este mensaje por completo?';
$lang['activate_inbox'] = '¿Estás seguro de que quieres hacer este Starred?';
$lang['deactivate_inbox'] = '¿Estás seguro de que quieres desstarrar esto?';

// Navigation
$lang['settings'] = 'Ajustes';
$lang['site'] = 'Sitio';
$lang['mail'] = 'Correo';
$lang['profile'] = 'Perfil';
$lang['logout'] = 'Cerrar sesión';

// General
$lang['list'] = 'Lista';
$lang['add'] = 'Añadir';
$lang['edit'] = 'Editar';

// Panel
$lang['online'] = 'En línea';
$lang['sidenav_header_1'] = 'Cuentas';
$lang['sidenav_header_2'] = 'administración';
$lang['sidenav_header_3'] = 'Perfil de la parte delantera';
$lang['sidenav_header_4'] = 'Comunicación';
$lang['dashboard'] = 'Tablero';
$lang['freelancer'] = 'Talento';
$lang['freelancers'] = 'Talentos';
$lang['client'] = 'Cliente';
$lang['clients'] = 'Clientes';
$lang['admin'] = 'Administración';
$lang['category'] = 'categoría';
$lang['categories'] = 'Categorías';
$lang['skill'] = 'Habilidad';
$lang['skills'] = 'Habilidades';

//Client Portal
$lang['job'] = 'Trabajo';
$lang['jobs'] = 'Trabajos';
$lang['post'] = 'Enviar';
$lang['a'] = 'un';

//Freelance Portal
$lang['bid'] = 'Oferta';
$lang['bids'] = 'Ofertas';
$lang['overview'] = 'Visión de conjunto';
$lang['portfolio'] = 'portafolio';
$lang['services'] = 'Talentos';
$lang['service'] = 'Servicio';
$lang['team'] = 'Equipo';
$lang['teammate'] = 'Compañero de equipo';
$lang['facebook_url'] = 'Facebook URL';
$lang['twitter_url'] = 'Twitter URL';
$lang['linkedin_url'] = 'LinkedIn URL';




$lang['documentation'] = 'Documentación';
$lang['all_rights'] = 'Todos los derechos reservados';

//Inside Details
$lang['control_panel'] = 'Panel de control';
$lang['home'] = 'Inicio';
$lang['business_ideas'] = 'Ideas de negocio';
$lang['no_content_found'] = 'No se ha encontrado ningún contenido';
$lang['more_info'] = 'Más información';
$lang['your'] = 'Tu';
$lang['section'] = 'Sección';
$lang['no_results'] = 'No hay resultados';
$lang['image'] = 'Imagen';
$lang['change'] = 'Cambio';
$lang['details'] = 'Detalles';
$lang['choose'] = 'Escoger';
$lang['file'] = 'Archivo';
$lang['default_profile'] = 'Imagen de perfil predeterminada';
$lang['panel_one'] = 'Panel de imagen uno';
$lang['panel_two'] = 'Panel de imagen dos';
$lang['bg_image'] = 'Imagen de fondo';
$lang['approved'] = 'Aprobado';
$lang['waiting'] = 'Esperando';
$lang['approval'] = 'Aprobación';

//Admin Settings
$lang['site_name'] = 'Este es el nombre del sitio.';
$lang['site_tagline'] = 'Este es el lema del sitio.';
$lang['site_url'] = 'Este es el enlace URL de su sitio.';
$lang['site_email'] = 'Este es el correo electrónico del sitio.';
$lang['site_email_pass'] = 'Esta es la contraseña del correo electrónico del sitio.';
$lang['site_description'] = 'Esta es la meta descripción del sitio.';
$lang['site_keywords'] = 'Esta es la meta meta palabras clave.';
$lang['site_author'] = 'Este es el meta-autor del sitio.';

//Install Settings
$lang['database_host'] = 'Host de base de datos';
$lang['database_name'] = 'Nombre de la base de datos';
$lang['database_username'] = 'Nombre de usuario de la base de datos';
$lang['database_password'] = 'Contraseña de base de datos';
$lang['install'] = 'Instalar';
$lang['go_back'] = 'Regresa';
$lang['go_home_page'] = 'Haga clic aquí para ir a la página principal';
$lang['next_step'] = 'Próximo paso';

//Install Errors
$lang['note'] = '¡Nota!';
$lang['connect_error'] = 'Error al conectarse a MySQL.';
$lang['install_success'] = 'La instalación The Kafe ha sido un éxito. Ahora, elimine o cambie el nombre de la carpeta de instalación.';
$lang['database_note'] = 'Asegúrese de haber creado una base de datos en su servidor antes de instalar la secuencia de comandos.';

?>
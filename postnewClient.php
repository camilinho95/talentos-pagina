<?php
//Check if init.php exists
if (!file_exists('core/frontinit.php')) {
	header('Location: install/');
	exit;
} else {
	require_once 'core/frontinit.php';
}
$query = DB::getInstance()->get("settings", "*", ["id" => 1]);
$url = 'http://talentosonline.co/';
$correo = '';
if ($query->count()) {
	foreach ($query->results() as $row) {
		$url = $row->url;
		$correo = $row->mail;
	}
}

if (Input::exists()) {
	if (Token::check(Input::get('token'))) {
		$errorHandler = new ErrorHandler;
		$validator = new Validator($errorHandler);
		$validation = $validator->check($_POST, [
			'name' => [
				'required' => true,
				'minlength' => 2,
				'maxlength' => 50
			],
			'email' => [
				'required' => true,
				'email' => true,
				'maxlength' => 100,
				'minlength' => 2,
				'unique' => 'freelancer',
				'unique' => 'client'
			],
			'username' => [
				'required' => true,
				'maxlength' => 20,
				'minlength' => 3,
				'unique' => 'freelancer',
				'unique' => 'client',
			]
		]);

		if (!$validation->fails()) {

			$client = new Client();
			$remember = (Input::get('remember') === 'on') ? true : false;
			$remitent = 'notifications@talentosonline.co';
			$salt = Hash::salt(32);
			$imagelocation = 'uploads/default.png';
			$clientid = uniqueid();
			try {

				$client->create(array(
					'clientid' => $clientid,
					'username' => $_POST['username'],
					'password' => Hash::make($_POST['contrasena'], $salt),
					'salt' => $salt,
					'name' => $_POST['name'],
					'email' => $_POST['email'],
					'phone' => $_POST['phone'],
					'imagelocation' => $imagelocation,
					'joined' => date('Y-m-d H:i:s'),
					'active' => 1,
					'user_type' => 1,
				));

				if ($client) {
					$titulo    = 'Bienvenido a Talentos';
					$mensaje   = '<div style="max-width:700px;"><div style="text-align:center;background-color:#382f84;"><img src="' . $url . 'assets/img/header/talentos_logos_white.png" style="width:60%"/></div>';
					$mensaje   .= 'Hola ' . $_POST['name'] . ' <br>' .
						'Gracias! Te damos la Bienvenida a Talentos. Hemos recibido tu solicitud y estamos gustosos de poder apoyarte en el requerimiento que tienes.
						<br> Estaremos contactándote en caso necesario de ampliar información y también para concretar tu orden con el talento especializado. <br>Estaremos resolviendo en un término de 24 horas. <br>
						 Sus credenciales de acceso son: <br>Nombre de usuario: ' . $_POST['email'] . '<br>' . 'Contraseña: ' . $_POST['contrasena'] . '</div>';
					$cabeceras = 'From: ' . $remitent . "\r\n" .
						'X-Mailer: PHP/' . phpversion() . "\r\n";
					// $headers .= "MIME-Version: 1.0\r\n";
					$cabeceras .= "Content-Type: text/html; charset=UTF-8\r\n";

					mail($_POST['email'], $titulo, $mensaje, $cabeceras);
					// $login = $client->login($_POST['email'], $_POST['contrasena'], $remember);
					echo "ok1 " . $clientid;
					$cabeceras = 'From: ' . $correo . "\r\n" .
						'X-Mailer: PHP/' . phpversion() . "\r\n";
					$cabeceras .= "Content-Type: text/html; charset=UTF-8\r\n";
					$titulo = 'Nuevo Usuario';
					$msj = 'Hay un nuevo usuario. Su correo es ' . $_POST['email'];
					mail($correo, $titulo, $msj, $cabeceras);
				} else {
					echo "err";
					$hasError = true;
				}
			} catch (Exception $e) {
				echo "error:";
				die($e->getMessage());
			}
		} else {
			foreach ($validation->errors()->all() as $err) {
				$str = implode(" ", $err);
				$error .= ' ' . $str;
			}
			echo  $error;
		}
	}
}

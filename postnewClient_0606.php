<?php
	//Check if init.php exists
	if(!file_exists('core/frontinit.php')){
		header('Location: install/');        
    	exit;
	}else{
 		require_once 'core/frontinit.php';	
	}
				$client = new Client();
				$remember = (Input::get('remember') === 'on') ? true : false;
				$remitent = 'notifications@talentosonline.co';
				$salt = Hash::salt(32);  
				$imagelocation = 'uploads/default.png';
				$clientid = uniqueid(); 
				try{
					
				  $client->create(array(
				   'clientid' => $clientid,
				   'username' => $_POST['user'] ,
				   'password' => Hash::make($_POST['contrasena'] , $salt),
				   'salt' => $salt,
				   'name' => $_POST['nombre'] ,
		           'email' => $_POST['email'] ,
				   'imagelocation' => $imagelocation,
		           'joined' => date('Y-m-d H:i:s'),
				   'active' => 1,
		           'user_type' => 1
				  ));	
				  
				if ($client) {
				    $titulo    = 'Bienvenido a Talentos';
                    $mensaje   = 'Hola '.$_POST['nombre'] .' \r\n'.
                                'Gracias! Te damos la Bienvenida a Talentos. Hemos recibido tu solicitud y estamos gustosos de
poder apoyarte en el requerimiento que tienes. \r\n Estaremos contactándote en caso necesario de ampliar información y también para concretar tu
orden con el talento especializado. \r\n Estaremos resolviendo en un término de 24 horas. \r\n
Sus credenciales de acceso son: \r\n Nombre de usuario: '.$_POST['user'].' \r\n'. 'Contraseña: '.$_POST['contrasena'] ;
                    $cabeceras = 'From: '. $remitent . "\r\n" .
                        'X-Mailer: PHP/' . phpversion();
                    
                    mail($_POST['email'], $titulo, $mensaje, $cabeceras);
			     $login = $client->login($_POST['email'], $_POST['contrasena'], $remember);
				 echo "ok ".$clientid;
			    }else {
			     echo "err";
			     $hasError = true;
			   }
					
				}catch(Exception $e){
				 echo "error:";
				 die($e->getMessage());	
				}
	
?>
<?php
require_once 'core/frontinit.php';

$fileName = uniqid();
$target_dir_file = "./imageService/";
$target_dir_mail = "/imageService/";
if (isset($_FILES["inscriptionFile"]['name'])) {
	$target_file = $target_dir_file . $fileName . '.' . pathinfo($_FILES["inscriptionFile"]['name'], PATHINFO_EXTENSION);
	$target_mail = $target_dir_mail . $fileName . '.' . pathinfo($_FILES["inscriptionFile"]['name'], PATHINFO_EXTENSION);
	move_uploaded_file($_FILES["inscriptionFile"]["tmp_name"], $target_file);
}

$cabeceras = 'From: notifications@talentosonline.co' . "\r\n" .
	'X-Mailer: PHP/' . phpversion() . "\r\n";
$cabeceras .= "Content-Type: text/html; charset=UTF-8\r\n";


$titulo    = 'Propuesta';
$mensaje    = '<div style="max-width:700px;"><div style="text-align:center;background-color:#382f84;">';
$mensaje    .= '<img src="http://talentosonline.co/assets/img/header/talentos_logos_white.png" style="width:60%"/></div>';
$mensaje    .= 'Hay una nueva propuesta en la plataforma llamado ' . $_POST['title'];
$mensaje     .= ' <br><a target="_blank" href="http://talentosonline.co' . $target_mail . '">Archivo adjunto</a>';
$mensaje    .= '</div>';


mail("notifications@talentosonline.co", $titulo, $mensaje, $cabeceras);

//Check if init.php exists
if (!file_exists('core/frontinit.php')) {
	header('Location: install/');
	exit;
} else {
	require_once 'core/frontinit.php';
}

// Aquí se deberían validar los datos ingresados por el usuario
if (
	!isset($_POST['title']) ||
	!isset($_POST['category']) ||
	!isset($_POST['job_type']) ||
	!isset($_POST['budget']) ||
	//!isset($_POST['start_date'])||
	//!isset($_POST['end_date'])||
	!isset($_POST['description']) ||
	!isset($_POST['correo'])
) {

	echo "<b>Ocurrió un error y el formulario no ha sido enviado. </b><br />";
	echo "Por favor, vuelva atrás y verifique la información ingresada<br />";
} else {
	try {
		$correo = $_POST['correo'];
		$remitent = 'notifications@talentosonline.co';
		$jobid = uniqueid();
		$skills = '';
		$slug = seoUrl($_POST['title']);
		$jobInsert = DB::getInstance()->insert('job', array(
			'description' => $_POST['description'],
			'jobid' => $jobid,
			'clientid' => $_POST['idclient'],
			'catid' => $_POST['category'],
			'title' => $_POST['title'],
			'photo' => "$target_dir_mail ". $fileName,
			'slug' => $slug,
			'country' => 'Colombia',
			'adress' => $_POST['address'],
			'job_type' => $_POST['job_type'],
			'budget' => $_POST['budget'],
			'start_date' => '', //$_POST['start_date'],
			'end_date' => '', //$_POST['end_date'],
			'skills' => $skills,
			'active' => 1,
			'delete_remove' => 0,
			'public' => 0,
			'invite' => 0,
			'date_added' => date('Y-m-d H:i:s')
		));

		if (count($jobInsert) > 0) {

			echo true;
		} else {
			echo false;
		}
	} catch (Exception $e) {
		die($e->getMessage());
	}
}

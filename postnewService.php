<?php
require_once 'core/frontinit.php';
$fileName = uniqid();
$target_dir_file = "./uploads/";
$target_dir_mail = "/uploads/";
$target_file = $target_dir_file . $fileName . '.' . pathinfo($_FILES["inscriptionFile"]['name'], PATHINFO_EXTENSION);
$target_mail = $target_dir_mail . $fileName . '.' . pathinfo($_FILES["inscriptionFile"]['name'], PATHINFO_EXTENSION);
move_uploaded_file($_FILES["inscriptionFile"]["tmp_name"], $target_file);

$cabeceras = 'From: notifications@talentosonline.co' . "\r\n" .
	'X-Mailer: PHP/' . phpversion() . "\r\n";
$cabeceras .= "Content-Type: text/html; charset=UTF-8\r\n";

$titulo    = 'Talento nuevo';


$mensaje = '<div style="max-width:700px;"><div style="text-align:center;background-color:#382f84;">';
$mensaje .= '<img src="http://talentosonline.co/assets/img/header/talentos_logos_white.png" style="width:60%"/></div>';
$mensaje .= 'Hay un nuevo talento. Descarga su hoja de vida en Este ENLACE: <a target="_blank" href="http://talentosonline.co' . $target_mail . '">HOJA DE VIDA</a>';
$mensaje .= '</div>';

mail("notifications@talentosonline.co", $titulo, $mensaje, $cabeceras);

/*$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["file"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["file"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["file"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}*/

// Aquí se deberían validar los datos ingresados por el usuario
if (
	!isset($_POST['title']) ||
	!isset($_POST['category']) ||
	!isset($_POST['rate']) ||
	!isset($_POST['service_desc'])
) {

	echo "<b>Ocurrió un error y el formulario no ha sido enviado. </b><br />";
	echo "Por favor, vuelva atrás y verifique la información ingresada<br />";
} else {
	try {
		$serviceid = uniqid();
		$serviceInsert = DB::getInstance()->insert('service', array(
			'description' => $_POST['service_desc'],
			'serviceid' => $serviceid,
			'userid' => $_POST['idtalent'],
			'catid' => $_POST['category'],
			'title' => $_POST['title'],
			'rate' => $_POST['rate'],
			'active' => 1,
			'delete_remove' => 0,
			'date_added' => date('Y-m-d H:i:s'),
			'type_rate' => $_POST['type_rate']
		));

		if (count($serviceInsert) > 0) {
			echo true;
		} else {
			echo false;
		}
	} catch (Exception $e) {
		die("Error: " . $e->getMessage());
	}
}

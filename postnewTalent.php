
<?php
//Check if frontinit.php exists
if(!file_exists('core/frontinit.php')){
	header('Location: install/');        
    exit;
}else{
 require_once 'core/frontinit.php';	
}

$query = DB::getInstance()->get("settings", "*", ["id" => 1]);
if ($query->count()) {
    foreach($query->results() as $row) {
	    $correo = $row->mail;
	    $url=$row->url;
    }			
}

//Get Payments Settings Data
$q1 = DB::getInstance()->get("payments_settings", "*", ["id" => 1]);
if ($q1->count()) {
 foreach($q1->results() as $r1) {
 	$currency = $r1->currency;
 	$membershipid = $r1->membershipid;
 }			
}

//Getting Payement Id from Database
$query = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
if ($query->count() === 1) {
  $q1 = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
} else {
  $q1 = DB::getInstance()->get("membership_agency", "*", ["membershipid" => $membershipid]);
}
if ($q1->count() === 1) {
 foreach($q1->results() as $r1) {
  $bids = $r1->bids;
 }
}

if($membershipid != ''){
    
if (Input::exists()) {
 //if(Token::check(Input::get('token'))){
    $errorHandler = new ErrorHandler;
	
	$validator = new Validator($errorHandler);
	
	$validation = $validator->check($_POST, [
	  'name' => [
		 'required' => true,
		 'minlength' => 2,
		 'maxlength' => 50
	   ],
	  'email' => [
	     'required' => true,
	     'email' => true,
	     'maxlength' => 100,
	     'minlength' => 2,
	     'unique' => 'freelancer',
	     'unique' => 'client'
	  ],			 
	  'username' => [
	     'required' => true,
	     'maxlength' => 20,
	     'minlength' => 3,
	     'unique' => 'freelancer',
	     'unique' => 'client'
	  ]/*,
	   'password' => [
	     'required' => true,
	     'minlength' => 6
	   ],
	   'confirmPassword' => [
	     'match' => 'password'
	   ]*/
	]);
	 	
	  if (!$validation->fails()) {
		  	$freelancer = new Freelancer();
		  
				$remember = (Input::get('remember') === 'on') ? true : false;
				$salt = 'asdasd';//Hash::salt(32);   
				$imagelocation = 'uploads/default.png';
				$bgimage = 'uploads/bg/default.jpg';
				$freelancerid = uniqueid(); 
				try{
				  $freelancer->create(array(
						'freelancerid' => $freelancerid,
						'username' => $_POST['username'],
						'password' => Hash::make($_POST['contrasena'], $salt),
						'salt' => $salt,
				    'name' => $_POST['name'],
						'email' => $_POST['email'],
						'phone' => '',
				    'imagelocation' => $imagelocation,
						'bgimage' => $bgimage,
						'tokencode' => '',
						'joined' => date('Y-m-d H:i:s'),
						'active' => 0,
						'delete_remove' => 0,
						'user_type' => 1,
						'membershipid' => $membershipid,
						'membership_bids' => $bids,
						'membership_date' => date('Y-m-d H:i:s'),
						'schedule_payments' => ''
				  ));	

				if ($freelancer) {
				    // $login = $freelancer->login($_POST['email'], $_POST['contrasena'], $remember);
                    
                    $cabeceras = 'From: '. $correo . "\r\n" .
                        'X-Mailer: PHP/' . phpversion()."\r\n";
                         $cabeceras .= "Content-Type: text/html; charset=UTF-8\r\n";
                    
                    $titulo    = 'Bienvenido a Talentos';
                    $mensaje='<div style="max-width:700px;"><div style="text-align:center;background-color:#382f84;">';
                    $mensaje.='<img src="'.$url.'assets/img/header/talentos_logos_white.png" style="width:60%"/></div>';
                    $mensaje.= '<label>Bienvenido a portal de talentos evaluaremos tu postulación y te enviaremos un mail</label><br>';
                    $mensaje.='<label>Sus credenciales de acceso son: <br>Nombre de usuario: '.$_POST['email'].'<br>'. 'Contraseña: '.$_POST['contrasena'].'</label></div>';
                    mail($_POST['email'], $titulo, $mensaje, $cabeceras);
                    
                    // $titulo    = 'Talento nuevo';
                    // $mensaje='<div style="max-width:700px;"><div style="text-align:center;background-color:#382f84;">';
                    // $mensaje.='<img src="'.$url.'assets/img/header/talentos_logos_white.png" style="width:60%"/></div>';
                    // $mensaje.= 'Hay un nuevo talento. Su correo es '.$_POST['email'].' puede descargarlo en <a target="_blank" href="http://talentosonline.co/uploads/'. $target_file .'">HOJA DE VIDA</a>';
                    // $mensaje.='</div>';
                    
                    // mail($correo, $titulo, $mensaje, $cabeceras);
                	echo "ok1 :".$freelancerid;
			    }else {
			    	echo "Ocurrio un error durante el registro";
			     $hasError = true;
			   }
					
				}catch(Exception $e){
					echo "err :".$e->getMessage();
				 die($e->getMessage());	
				}	
			} else {
			    foreach ($validation->errors()->all() as $err) {
        	     	$str = implode(" ",$err);
        	     	$error .= ' '.$str;
	            }
	            echo  $error;
			     $hasError = true;
			}
    /* }   else {
    				  echo "err :Error de membresia";
    				  $memError = true;
    } */
 }
}

?>
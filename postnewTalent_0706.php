
<?php
//Check if frontinit.php exists
if(!file_exists('core/frontinit.php')){
	header('Location: install/');        
    exit;
}else{
 require_once 'core/frontinit.php';	
}

$query = DB::getInstance()->get("settings", "*", ["id" => 1]);
if ($query->count()) {
    foreach($query->results() as $row) {
	    $correo = $row->mail;
    }			
}

//Get Payments Settings Data
$q1 = DB::getInstance()->get("payments_settings", "*", ["id" => 1]);
if ($q1->count()) {
 foreach($q1->results() as $r1) {
 	$currency = $r1->currency;
 	$membershipid = $r1->membershipid;
 }			
}

//Getting Payement Id from Database
$query = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
if ($query->count() === 1) {
  $q1 = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
} else {
  $q1 = DB::getInstance()->get("membership_agency", "*", ["membershipid" => $membershipid]);
}
if ($q1->count() === 1) {
 foreach($q1->results() as $r1) {
  $bids = $r1->bids;
 }
}

if($membershipid != ''){
		  	  	$freelancer = new Freelancer();
		  
				$remember = (Input::get('remember') === 'on') ? true : false;
				$salt = Hash::salt(32);  
				$imagelocation = 'uploads/default.png';
				$bgimage = 'uploads/bg/default.jpg';
				$freelancerid = uniqueid(); 
				try{
					
				  $freelancer->create(array(
				   'freelancerid' => $freelancerid,
				   'username' => $_POST['user'],
				   'password' => Hash::make($_POST['contrasena'], $salt),
				   'salt' => $salt,
				   'name' => $_POST['nombre'],
		           'email' => $_POST['email'],
				   'imagelocation' => $imagelocation,
				   'bgimage' => $bgimage,
		           'membershipid' => $membershipid,
		           'membership_bids' => $bids,
		           'membership_date' => date('Y-m-d H:i:s'),
		           'joined' => date('Y-m-d H:i:s'),
				   'active' => 0,
		           'user_type' => 1
				  ));	
				  
				if ($freelancer) {
				    $login = $freelancer->login($_POST['email'], $_POST['contrasena'], $remember);
                    $titulo    = 'Talento nuevo';
                    $mensaje   = 'Hay un nuevo talento. Su correo es '.$_POST['email'];
                    $cabeceras = 'From: '. $correo . "\r\n" .
                        'X-Mailer: PHP/' . phpversion();
                    
                    mail($correo, $titulo, $mensaje, $cabeceras);
                	echo "ok :".$freelancerid;
			    }else {
			    	echo "err :Ocurrio un error durante el registro";
			     $hasError = true;
			   }
					
				}catch(Exception $e){
					echo "err :".$e->getMessage();
				 die($e->getMessage());	
				}	
			}else {
				  echo "err :Error de membresia";
				  $memError = true;
			}

?>
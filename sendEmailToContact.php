<?php
require_once 'core/frontinit.php';

$cabeceras = 'From:'. $_POST['email'] . "\r\n" .
	'X-Mailer: PHP/' . phpversion() . "\r\n";
$cabeceras .= "Content-Type: text/html; charset=UTF-8\r\n";

$titulo    = 'Nuevo Formulario de contacto';
$mensaje    = '<div style="max-width:700px;"><div style="text-align:center;background-color:#382f84;">';
$mensaje    .= '<img src="http://talentosonline.co/assets/img/header/talentos_logos_white.png" style="width:60%"/></div>';
$mensaje    .= '<h2>Nombre del contacto: ' . $_POST['name'];
$mensaje    .= '<br>Mensaje: ' . $_POST['message'];
$mensaje    .= '</div>';

mail("notifications@talentosonline.co", $titulo, $mensaje, $cabeceras);

header("Location: index.php");

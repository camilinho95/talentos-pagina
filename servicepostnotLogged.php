<?php
//Check if init.php exists
if (!file_exists('core/frontinit.php')) {
  header('Location: install/');
  exit;
} else {
  require_once 'core/frontinit.php';
}

?>

<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="es">
<!--<![endif]-->
<link href="assets/css/nostyle.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap 3.3.6-->
<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- Font-awesome 4.5.0-->
<link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Data Tables -->
<link href="assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- Summernote -->
<link href="assets/css/summernote.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap Datetimepicker CSS -->
<link href="assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap Select CSS-->
<link href="assets/css/bootstrap-select.css" rel="stylesheet" type="text/css" />



<!-- Include header.php. Contains header content. -->
<?php include('includes/template/header.php'); ?>

<body class="greybg">

  <!-- Include navigation.php. Contains navigation content. -->
  <?php include('includes/template/navigation.php'); ?>

  <!-- ==============================================
	 Header
	 =============================================== -->
  <header class="header-jobs" style="

      background: linear-gradient(
          rgba(34,34,34,0.7), 
          rgba(34,34,34,0.7)
        ), url('assets/img/service/1542320014ndo trabajos.png') no-repeat center center fixed;
      background-size: cover;
      background-position: center center;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      color: #fff;
      height: 120vh;
      width: 100%;
      ">
    <meta http-equiv="Content-Type" content="text/html; charset=euc-jp">

    <div class="container">
      <!--Holaaaaa mundo-->
      <label>Hola mundo</label>
      <!--Hola mundo fin-->
      <div class="row">
        <div class="content">
          <div class="box-header">
            <h3 class="box-title">Escriba su profesión o talento (ej: Músico)</h3>
            <!--<h3 class="box-title"><?php //echo $lang['add']; 
                                      ?> <?php //echo $lang['job']; 
                                                                    ?></h3>-->
          </div>

          <form role="form" method="POST" id="addform" action="postnewService.php" enctype="multipart/form-data">

            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-info"></i></span>
                <input type="text" name="title" class="form-control" placeholder="<?php echo $lang['talento']; ?>" value="<?php echo escape(Input::get('title')); ?>" id="title-form" required />
              </div>
            </div>

            <div class="form-group">
              <label><?php echo $lang['category']; ?></label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-pencil-square"></i></span>
                <select name="category" type="text" class="form-control">
                  <?php
                  $query = DB::getInstance()->get("category", "*", ["AND" => ["active" => 1, "delete_remove" => 0]]);
                  if ($query->count()) {
                    $categoryname = '';
                    $x = 1;
                    foreach ($query->results() as $row) {
                      echo $categoryname .= '<option value = "' . $row->catid . '">' . $row->name . '</option>';
                      unset($categoryname);
                      $x++;
                    }
                  }
                  ?>
                </select>
              </div>
            </div>

            <!--<div class="button-next-step box-footer" id="btn-next-step-1"> <!--BOTON SIGUIENTE PASO -->
            <!--<button  class="btn btn-primary" id="next-1"><?php echo $lang['next_step']; ?></button>
      </div>-->

            <div class="content-form-animated-I form-group" style="margin-bottom:0px;">
              <label><?php echo $lang['rate']; ?></label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                <input type="text" name="rate" class="form-control" placeholder="<?php echo $lang['rate']; ?>" value="<?php echo escape(Input::get('rate')); ?>" />
              </div>
            </div>
            <div class="content-form-animated-I form-group">
              <div style="display: flex">
                <div style="margin-top:5px; margin-left:5px;"> <label> <input type="radio" name="type_rate" value="mensual" data-tokens="mensual" checked>Trabajo completo</label> </div>
                <div style="margin-top:5px; margin-left:5px;"> <label> <input type="radio" name="type_rate" value="hora" data-tokens="hora">Por hora</label> </div>
              </div>
            </div>

            <div class="content-form-animated-I form-group">
              <label>Descargue el formulario de inscripcion AQUI:</label>
              <a href="http://talentosonline.co/DescargarFormularioInscripcion.php">Descargar</a>
            </div>

            <div class="content-form-animated-I form-group">
              <label><?php echo $lang['description']; ?></label>
              <p>Denos una breve descripción de los servicios que ofrece y sus fortalezas en esta área.</p>
              <!--<textarea type="text" id="summernote" name="service_desc" class="form-control" required>
                       <?php echo escape($service_desc); ?></textarea>-->
              <textarea type="text" name="service_desc" class="form-control" required><?php echo escape($service_desc); ?></textarea>
            </div>

            <div class="form-group" style="padding-bottom:10px;">
              <label>Sube formulario de inscripcion o tu hoja de vida en pdf:</label><br>
              <input type="file" name="inscriptionFile" id="file">
            </div>

            <!--<a href="assets/jsForms/DownloadFiles.php?archivo="hola_mundo.txt" >Descargar Archivo</a>-->

            <div class="content-form-animated-I box-footer">
              <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
              <?php
              if ($freelancer->isLoggedIn()) { ?>
                <input type="hidden" name="logged" value="1" id="logged" />
                <input type="hidden" name="idtalent" value="<?php
                                                            echo $freelancer->data()->freelancerid . "" ?>" id="idtalent" />
              <?php } else { ?>
                <input type="hidden" name="logged" value="0" id="logged" />
                <input type="hidden" name="idtalent" value="" id="idtalent" />
              <?php }
              ?>
              <button type="submit" name="data" class="btn btn-primary full-width"><?php echo $lang['submit']; ?></button>
            </div>
          </form>
          <p class="statusFormJob"></p>
        </div><!-- /.row -->
      </div><!-- /.content -->

      <div class="modal" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Formulario de Registro</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
              <p class="statusMsg"></p>
              <form role="form">
                <div class="form-group">
                  <label for="inputNombre">Nombre completo</label>
                  <input type="text" class="form-control" id="inputNombre" placeholder="Ingrese su nombre completo" />
                </div>
                <div class="form-group">
                  <label for="inputEmail">Email</label>
                  <input type="email" class="form-control" id="inputEmail" placeholder="Ingrese su email" />
                </div>
                <div class="form-group">
                  <label for="inputUser">Nombre de usuario</label>
                  <input type="text" class="form-control" id="inputUser" placeholder="Ingrese su telefono" />
                </div>
                <input type="hidden" name="token" id="token" value="<?php echo Token::generate(); ?>" />
                <!--<div class="form-group">
                        					<label for="inputContrasena">Contraseña</label>
                        					<input type="password" class="form-control" id="inputContrasena" placeholder="Contraseña"/>
                    					</div>
                    					<div class="form-group">
                        					<label for="inputContrasena2">Confirmar contraseña</label>
                        					<input type="password" class="form-control" id="inputContrasena2" placeholder="Confirme su contraseña"/>
                    					</div>-->
              </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary submitBtn" onclick="submitTalentForm()">Enviar</button>
            </div>

          </div>
        </div>
      </div>
    </div><!-- /.container -->

  </header><!-- /header -->
  <div>
    <!-- Include footer.php. Contains footer content. -->
    <?php include 'includes/template/footer.php'; ?>
  </div>
  <!-- ==============================================
	 Scripts
	 =============================================== -->

  <!-- jQuery 2.1.4 -->
  <script src="assets/js/jQuery-2.1.4.min.js" type="text/javascript"></script>
  <!-- Bootstrap 3.3.6 JS -->
  <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

  <!-- AdminLTE App -->
  <script src="assets/js/app.min.js" type="text/javascript"></script>


  <!-- Summernote WYSIWYG-->
  <script src="assets/js/summernote.min.js" type="text/javascript"></script>

  <script src="assets/js/animation-form.js?v=<?php echo time(); ?>" type="text/javascript"></script>
  <script language="javascript">
    alert("Al ingresar a Talentos está autorizando el uso de datos personales de acuerdo a lo señalado por la ley 1581 de 2012 y decreto reglamentario 1377 de 2013.");
  </script>
  <script>
    $(document).ready(function() {
      $('#summernote').summernote({
        height: 300, // set editor height

        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor

        focus: true, // set focus to editable area after initializing summernote
      });
    });
  </script>
</body>

</html>
<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
	header('Location: install/');        
    exit;
}else{
 require_once 'core/frontinit.php';	
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> 
<!--<![endif]-->
	
    <!-- Include header.php. Contains header content. -->
    <?php include ('includes/template/header.php'); ?> 

<body class="greybg">
	
     <!-- Include navigation.php. Contains navigation content. -->
 	 <?php include ('includes/template/navigation.php'); ?> 
 	 
      <!-- ==============================================
	 Header
	 =============================================== -->	 
	 <header class="header-about" style="

  background: url('<?php echo $about_header_img; ?>') no-repeat center center fixed;
  background-size: cover;
  background-position: center center;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  color: #fff;
  height: 65vh;
  width: 100%;
  
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;">
	   <div class="content">
        <div class="row">
	     <h1 class="revealOnScroll" data-animation="fadeInDown">
	       	<?php if($use_icon === '1'): ?>
	       		<i class="fa <?php echo $site_icon; ?>"></i>
	       	<?php endif; ?>  <?php echo $lang['about']; ?> <?php echo $lang['us']; ?></h1>
		 <p><?php echo $about_top_title; ?></p>
        </div><!-- /.row -->
       </div><!-- /.content -->
     </header><!--./header -->	
     
     <!-- ==============================================
	 Hello Section
	 =============================================== -->
     	<div class="container">
     		
     		<h1>ZAPATERO A SUS ZAPATOS…</h1>
 
Eres de los que tratas de ahorrar dinero y tiempo creyendo que todo lo puedes hacer o que, si sabes arreglar un daño en la tubería de tu baño, también puedes arreglar la batería de tu auto? O simplemente buscas en internet un tutorial y listo!
Deja que los profesionales se encarguen de las cosas, si, es un presupuesto con el que tal vez no contabas, pero piensa en el costo beneficio que te traerá a largo plazo. Como dicen por ahí, zapatero a sus zapatos…
De verdad no ahorras dinero ni tiempo tratando de arreglar el mismo problema que te persigue cada ocho días.  No dejes para mañana lo que puedes hacer hoy, cotiza con un verdadero profesional.
Pongamos este panorama, como un simple ejemplo: Vives sola o solo, llegas cansada o cansado de trabajar y te pones a organizar tu apartamento porque ya no aguantas el polvo y en 5 días el tiempo no te ha dado chico de medio arreglar. O eres de las que no sale el día domingo por quedarse limpiando y organizando tu departamento. ¡No!! Te lo mereces, descansa, dedícale tiempo a otros asuntos. Contrata un experto para que haga lo que necesites que haga. Así estarás  dándole la oportunidad a una persona que necesita el trabajo. Seguramente a ti te contrataron para hacer lo que sabes hacer. Bueno, pues alguien afuera esta esperando por ser contratado para hacer algo que tú necesitas que haga.
Ves, en temas de mecánica, construcción, hogar y otros tantos también es bueno contar con un profesional.
Nuestros talentos inscritos son reconocidos por la experiencia, pues ya hemos hecho una labor de investigación suficiente y confiable para exponerlos al mundo entero con lo que saben hacer.
En la página de talentos encontraras diferentes cotizaciones a precios justos y flexibles según tus requerimientos. Te invitamos a ingresar, registrarte y cotizar. 

     	</div>


  	 
	 
     <!-- Include footer.php. Contains footer content. -->
 	 <?php include ('includes/template/footer.php'); ?> 
	 
     <a id="scrollup">Scroll</a>
	 
     <!-- ==============================================
	 Scripts
	 =============================================== -->
	 
     <!-- jQuery 2.1.4 -->
     <script src="assets/js/jQuery-2.1.4.min.js" type="text/javascript"></script>
     <!-- Bootstrap 3.3.6 JS -->
     <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
     <!-- Kafe JS -->
     <script src="assets/js/kafe.js" type="text/javascript"></script>

</body>
</html>
